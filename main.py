import os


#Egor : удаляет файл
def delete_file():
	file_path = 'file.txt'
	try:
		os.remove(file_path)
		return True
	except:
		print("The system cannot find file specified")
		return False

#Marina: читает файл
def read_txt_file():
    file = 'file.txt'
    with open(file,'r',encoding='utf-8') as f:
        data = f.read()
        print(data)
        return data 

#Nikita : write in file
def write_in_txt_file():
	with open('test.txt', 'w') as writer:
		writer.write('I love Git')
		return True


if __name__ == "__main__":
	if write_in_txt_file(): # create and write in file "I love Git"
		print("[OK[ write")

	print(read_txt_file()) # read all

	if delete_file(): #delet file
		print("[OK] delete")



